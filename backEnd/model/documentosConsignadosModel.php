<?php
    /**
     * Clase Documento
     * 
     * @property Int $id identificacion del documento
     * @property Int $id_aspirante identificacion del usuario
     * @property String $copia_cedula indica si entrego o no este documento
     * @property String $copia_partida indica si entrego o no este documento
     * @property String $copia_titulo indica si entrego o no este documento
     * @property String $copia_certificacion indica si entrego o no este documento
     * @property String $certificado_opsu indica si entrego o no este documento
     * @property String $fondo_negro indica si entrego o no este documento
     */
    class DocumentosConsignados
    {
        public $id;
        public $id_aspirante;
        public $copia_cedula;
        public $copia_partida;
        public $copia_titulo;
        public $copia_certificacion;
        public $certificado_opsu;
        public $fondo_negro;

        function __construct()
		{
			$this->copia_cedula = 'N';
			$this->copia_partida = 'N';
			$this->copia_titulo = 'N';
			$this->copia_certificacion = 'N';
			$this->certificado_opsu = 'N';
			$this->fondo_negro = 'N';
		}

        public function registrar($array, $search = 1)
        {
            if ($search == 1) {
                include '../BaseDatos.php';
            } else {
                include 'BaseDatos.php';
            }
            $db = new BaseDatos();

            $this->id_aspirante = $array['id_aspirante'];
            
            if ($db->conectar()) {
                $sql = "SELECT * FROM documentos_consignados WHERE id_aspirante = ". $this->id_aspirante ." LIMIT 1";
                          
                $result = $db->conexion->query($sql);
                
                if ($result->num_rows > 0) {
                    $row = $result->fetch_assoc();
                    $this->copia_cedula = ($array['copia_cedula'] == 'S') ? $array['copia_cedula'] : $row['copia_cedula'];
                    $this->copia_partida = ($array['copia_partida'] == 'S') ? $array['copia_partida'] : $row['copia_partida'];
                    $this->copia_titulo = ($array['copia_titulo'] == 'S') ? $array['copia_titulo'] : $row['copia_titulo'];
                    $this->copia_certificacion = ($array['copia_certificacion'] == 'S') ? $array['copia_certificacion'] : $row['copia_certificacion'];
                    $this->certificado_opsu = ($array['certificado_opsu'] == 'S') ? $array['certificado_opsu'] : $row['certificado_opsu'];
                    $this->fondo_negro = ($array['fondo_negro'] == 'S') ? $array['fondo_negro'] : $row['fondo_negro'];
                    
                    $sql = "UPDATE documentos_consignados SET copia_cedula = \"". $this->copia_cedula ."\", copia_partida = \"". $this->copia_partida ."\", copia_titulo = \"". $this->copia_titulo ."\", copia_certificacion = \"". $this->copia_certificacion ."\", certificado_opsu = \"". $this->certificado_opsu ."\", fondo_negro = \"". $this->fondo_negro ."\" WHERE id_aspirante = ". $this->id_aspirante; 
                    // print_r($array);
                    // print_r($sql);
                    // die();
                } else {
                    $row = $result->fetch_assoc();
                    $this->copia_cedula = ($array['copia_cedula'] == 'S') ? $array['copia_cedula'] : $this->copia_cedula;
                    $this->copia_partida = ($array['copia_partida'] == 'S') ? $array['copia_partida'] : $this->copia_partida;
                    $this->copia_titulo = ($array['copia_titulo'] == 'S') ? $array['copia_titulo'] :  $this->copia_titulo;
                    $this->copia_certificacion = ($array['copia_certificacion'] == 'S') ? $array['copia_certificacion'] : $this->copia_certificacion;
                    $this->certificado_opsu = ($array['certificado_opsu'] == 'S') ? $array['certificado_opsu'] : $this->certificado_opsu;
                    $this->fondo_negro = ($array['fondo_negro'] == 'S') ? $array['fondo_negro'] :  $this->fondo_negro;

                    $sql = "INSERT INTO documentos_consignados (id_aspirante, copia_cedula, copia_partida, copia_titulo, copia_certificacion, certificado_opsu, fondo_negro) VALUES (". $this->id_aspirante .", \"". $this->copia_cedula ."\", \"". $this->copia_partida ."\", \"". $this->copia_titulo ."\", \"". $this->copia_certificacion ."\", \"". $this->certificado_opsu ."\", \"". $this->fondo_negro ."\");";
                }

                if ($db->conexion->query($sql) === TRUE) { 
                    $db->desconectar();                   
                    return true;
                } else {
                    echo "Error: " . $sql . "<br>" . $db->conexion->error;
                    $db->desconectar();
                    return false;
                }   
            }
            return false;
            
        }

    }
    
?>